let a = [1,2, null, 4, 5, undefined, false, '', 0]
let d = ['', 2,4,5,7,null, undefined, 'adfsafds']
function clear(arr){
    return arr.filter((value)=>{
        return value !== null && value !== undefined && value !== false && value !== ''
    })
}
console.log(clear(a))
console.log(clear(d))

// ----------------------//

function lengthOfStrings(strings = []){
    return strings.map((value)=>value.length)
}

console.log(lengthOfStrings(['5jhfghgf', 'daf', 'af']))
// -------------------------------------- //
function move(arr = [], from = 0, to = 1){
    const elementFrom = arr[from]
    arr[from] = arr[to]
    arr[to] = elementFrom
}

let b = [1,3,4,5,6,7,7,7,7]
console.log(b)
move(b, 3, 7)
console.log(b)